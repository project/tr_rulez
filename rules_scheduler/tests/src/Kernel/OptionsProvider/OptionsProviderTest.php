<?php

namespace Drupal\Tests\rules_scheduler\Kernel\OptionsProvider;

use Drupal\Core\Form\OptGroup;
use Drupal\KernelTests\KernelTestBase;
use Drupal\rules_scheduler\TypedData\Options\RulesComponentOptions;

/**
 * Tests use of option providers.
 *
 * @group Rules
 */
class OptionsProviderTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'tr_rulez',
    'rules',
    'typed_data',
    'rules_test_default_component',
  ];

  /**
   * The class resolver service used to instantiate options providers.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolver
   */
  protected $classResolver;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create the components defined by the test module.
    $this->installConfig(['rules_test_default_component']);

    // The core OptionsProviderResolver uses this service to instantiate
    // options providers when given a ::class.
    $this->classResolver = $this->container->get('class_resolver');
  }

  /**
   * Tests output of options providers.
   *
   * @param string $definition
   *   A string class constant to identify the options provider class to test.
   * @param array $options
   *   An associative array containing the 'value' => 'option' pairs expected
   *   from the options provider being tested.
   *
   * @dataProvider provideOptionsProviders
   */
  public function testOptionsProvider(string $definition, array $options): void {
    $provider = $this->classResolver->getInstanceFromDefinition($definition);

    $flatten_options = OptGroup::flattenOptions($options);
    $values = array_keys($flatten_options);

    $this->assertNotNull($provider);
    $this->assertEquals($options, $provider->getPossibleOptions());
    $this->assertEquals($values, $provider->getPossibleValues());
    $this->assertEquals($options, $provider->getSettableOptions());
    $this->assertEquals($values, $provider->getSettableValues());
  }

  /**
   * Data provider - provides test data for testOptionsProviders().
   */
  public static function provideOptionsProviders(): array {
    return [
      'Components' => [
        RulesComponentOptions::class, [
          'rules_test_default_component' => 'Rules test default component',
        ],
      ],
    ];
  }

}
