<?php

declare(strict_types=1);

namespace Drupal\Tests\rules_scheduler\Functional;

use Drupal\Tests\Traits\Core\CronRunTrait;
use Drupal\Tests\rules\Functional\RulesBrowserTestBase;
use Drupal\rules_scheduler\Entity\Task;

/**
 * Test cases for the Rules Scheduler module.
 *
 * @group Rules
 */
class RulesSchedulerTestCase extends RulesBrowserTestBase {
  use CronRunTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'rules',
    'rules_scheduler',
    'rules_scheduler_test',
  ];

  /**
   * Rules debug logger channel.
   *
   * @var \Drupal\rules\Logger\RulesDebugLoggerChannel
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->logger = $this->container->get('logger.channel.rules_debug');

    $config_factory = $this->container->get('config.factory');
    $rulesSettings = $config_factory->getEditable('rules.settings');
    $rulesSettings->set('debug_log.enabled', TRUE)->save();
  }

  /**
   * Tests that custom task handlers are properly invoked.
   */
  public function testCustomTaskHandler(): void {
    // Set up a scheduled task that will simply write a variable when executed.
    $variable = 'rules_scheduler_task_handler_variable';
    $task = Task::create([
      'date' => \Drupal::time()->getRequestTime(),
      'identifier' => 'rules_scheduler_test',
      'config' => '',
      'data' => ['variable' => $variable],
      'handler' => 'Drupal\rules_scheduler_test\TestTaskHandler',
    ]);
    $task->schedule();

    // Run cron. In hook_cron(), Rules Scheduler will queue the task, then cron
    // will use the queue API to run the workers to process the task.
    $this->cronRun();

    // The test task handler simply sets the passed variable to TRUE,
    // so test to see that the test task handler actually ran and worked.
    $config_factory = $this->container->get('config.factory');
    $testSettings = $config_factory->getEditable('rules_scheduler_test.settings');
    $this->assertTrue($testSettings->get($variable));
  }

}
