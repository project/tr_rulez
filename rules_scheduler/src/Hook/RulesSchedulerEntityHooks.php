<?php

declare(strict_types=1);

namespace Drupal\rules_scheduler\Hook;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\rules\Entity\RulesComponentConfig;

/**
 * Hook implementations used to create and dispatch Entity Events.
 */
final class RulesSchedulerEntityHooks {
  use StringTranslationTrait;

  /**
   * Constructs a new RulesSchedulerEntityHooks service.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(
    TranslationInterface $string_translation,
  ) {
    $this->stringTranslation = $string_translation;
  }

  /**
   * Implements hook_entity_operation_alter().
   *
   * Adds a 'schedule' operation for Action components.
   */
  #[Hook('entity_operation_alter')]
  public function entityOperationAlter(array &$operations, EntityInterface $entity): void {
    // We want to be more specific, perhaps RulesActionInterface here.
    if ($entity instanceof RulesComponentConfig) {
      $operations['schedule'] = [
        'title' => $this->t('Schedule'),
        'url' => Url::fromRoute('entity.rules_component.schedule', ['rules_component' => $entity->id()]),
        // Active selection is weight 0, 'edit' is 10, 'delete' is 100.
        'weight' => 25,
      ];
    }
  }

}
