<?php

declare(strict_types=1);

namespace Drupal\rules_scheduler\Hook;

use Drupal\Core\Hook\Attribute\Hook;
use Drupal\rules_scheduler\SchedulerManagerInterface;

/**
 * Hook implementations used for scheduled execution.
 */
final class RulesSchedulerCronHooks {

  /**
   * Constructs a new RulesSchedulerCronHooks service.
   *
   * @param \Drupal\rules_scheduler\SchedulerManagerInterface $schedulerManager
   *   The rules_scheduler.manager service.
   */
  public function __construct(
    protected SchedulerManagerInterface $schedulerManager,
  ) {}

  /**
   * Implements hook_cron().
   *
   * Finds tasks that are ready for execution and puts them in the queue.
   * Uses the 'rules_scheduler_tasks' queue.
   */
  #[Hook('cron')]
  public function cron(): void {
    $this->schedulerManager->queueTasks();
  }

}
