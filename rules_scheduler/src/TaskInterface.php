<?php

namespace Drupal\rules_scheduler;

/**
 * Provides an interface that defines the Task class.
 *
 * Tasks are created in memory using Task::create().
 * Tasks are 'scheduled' by saving them in the database using Task::schedule().
 * Tasks are retrieved from the database using Task::load().
 * Scheduled tasks will be queued for execution at the time returned by
 *   Task::getDate(). When a Task has been executed it will be deleted
 *   automatically, or you can delete it using Task::delete(). The execution
 *   and deletion of scheduled tasks is handled by the Drupal Queue API.
 * Task::loadReadyToRun() searches scheduled tasks and returns an array of
 *   Tasks which are ready to execute, based on the scheduled datetime and the
 *   current datetime.
 */
interface TaskInterface {

  /**
   * Returns a unique sequence number for tasks.
   *
   * @return int
   *   The task sequence number.
   */
  public function id(): int;

  /**
   * Sets the task identifier to the given value.
   *
   * @param string $identifier
   *   The task identifier.
   *
   * @return $this
   */
  public function setIdentifier(string $identifier): static;

  /**
   * Returns the identifier of this task.
   *
   * @return string
   *   The task identifier.
   */
  public function getIdentifier(): string;

  /**
   * Sets the scheduled time timestamp of this task.
   *
   * @param int $date
   *   The name of this order status.
   *
   * @return $this
   */
  public function setDate(int $date): static;

  /**
   * Returns the scheduled time timestamp of this task.
   *
   * @return int
   *   The scheduled time of this status.
   */
  public function getDate(): int;

  /**
   * Sets the name of the task component to the given value.
   *
   * @param string $config
   *   The name of the task component.
   *
   * @return $this
   */
  public function setConfig(string $config): static;

  /**
   * Returns the name of the task component.
   *
   * @return string
   *   The name of the task component.
   */
  public function getConfig(): string;

  /**
   * Sets any additional data to store with the task.
   *
   * @param object $data
   *   Object holding the additional data.
   *
   * @return $this
   */
  public function setData(object $data): static;

  /**
   * Returns any additional data to store with the task.
   *
   * @return object
   *   Object holding the additional data.
   */
  public function getData(): object;

  /**
   * Sets the task handler class name.
   *
   * @param string $handler
   *   The fully-qualified name of the task handler class.
   *
   * @return $this
   */
  public function setHandler(string $handler): static;

  /**
   * Returns the task handler class name.
   *
   * @return string
   *   The fully-qualified name of the task handler class.
   */
  public function getHandler(): string;

  /**
   * Creates a Task, but does not schedule it.
   *
   * @param array $values
   *   (optional) Array of initialization values.
   *
   * @return \Drupal\rules_scheduler\TaskInterface
   *   An object implementing TaskInterface.
   */
  public static function create(?array $values): TaskInterface;

  /**
   * Loads a scheduled task.
   *
   * @param int $tid
   *   A task unique sequence number.
   *
   * @return \Drupal\rules_scheduler\TaskInterface|null
   *   A task object, or NULL if there isn't one.
   */
  public static function load(int $tid): ?TaskInterface;

  /**
   * Loads all tasks that are ready to execute.
   *
   * "Ready to run" means that the 'now' datetime is greater than or equal to
   * the task's scheduled time.
   *
   * @return \Drupal\rules_scheduler\TaskInterface[]
   *   An array of task objects.
   */
  public static function loadReadyToRun(): array;

  /**
   * Schedules this task to be executed later on.
   */
  public function schedule(): void;

  /**
   * Deletes the task.
   */
  public function delete(): void;

}
