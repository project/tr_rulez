<?php

namespace Drupal\rules_scheduler;

/**
 * Default scheduled task handler.
 */
class DefaultTaskHandler implements TaskHandlerInterface {

  /**
   * The task.
   *
   * @var \Drupal\rules_scheduler\TaskInterface
   */
  protected $task;

  /**
   * Constructs a repetitive task handler object.
   */
  public function __construct(TaskInterface $task) {
    $this->task = $task;
  }

  /**
   * {@inheritdoc}
   */
  public function runTask(): void {
    // Get the execution state from the task.
    $data = $this->task->getData();

    /* @todo Implement! */
  }

  /**
   * {@inheritdoc}
   */
  public function afterTaskQueued(): void {
    // Delete the task from the task list.
    $this->task->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function getTask(): TaskInterface {
    return $this->task;
  }

}
