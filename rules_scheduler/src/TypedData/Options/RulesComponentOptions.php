<?php

declare(strict_types=1);

namespace Drupal\rules_scheduler\TypedData\Options;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\rules\TypedData\Options\OptionsProviderBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Options provider to list all Rules Components.
 */
class RulesComponentOptions extends OptionsProviderBase implements ContainerInjectionInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a RulesComponentOptions object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleOptions(?AccountInterface $account = NULL) {
    $options = [];
    $components = $this->configFactory->listAll('rules.component');
    foreach ($components as $component) {
      $config = $this->configFactory->get($component);
      $options[$config->get('id')] = $config->get('label');
    }

    // Sort the result by value for ease of locating and selecting.
    asort($options);

    return $options;
  }

}
