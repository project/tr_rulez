<?php

declare(strict_types=1);

namespace Drupal\rules_scheduler\Drush\Commands;

use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\RequeueException;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\rules_scheduler\SchedulerManagerInterface;
use Drush\Attributes as CLI;
use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;

// cspell:ignore rusch

/**
 * Drush 12+ commands for the Rules Scheduler module.
 */
final class RulesSchedulerDrushCommands extends DrushCommands {
  use AutowireTrait;

  /**
   * RulesSchedulerDrushCommands constructor.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue factory service.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queueWorkerManager
   *   The queue worker manager service.
   * @param \Drupal\rules_scheduler\SchedulerManagerInterface $rulesSchedulerManager
   *   The rules scheduler manager.
   */
  public function __construct(
    protected QueueFactory $queueFactory,
    protected QueueWorkerManagerInterface $queueWorkerManager,
    protected SchedulerManagerInterface $rulesSchedulerManager,
  ) {
    parent::__construct();
  }

  /**
   * Command callback for processing the 'rules_scheduler_tasks' queue.
   *
   * Checks for scheduled tasks to be added to the queue.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases,
   *   config, etc.
   *
   * @command rules:scheduler-tasks
   * @aliases rusch,rules-scheduler-tasks
   *
   * @option claim
   *   Optionally claim tasks from the queue to work on. Any value set will
   *   override the default time spent on this queue.
   *
   * @usage drush rules:scheduler-tasks
   *   Add scheduled tasks to the queue.
   * @usage drush rules:scheduler-tasks --claim
   *   Add scheduled tasks to the queue and claim items for the default amount
   *   of time.
   * @usage drush rules:scheduler-tasks --claim=30
   *   Add scheduled tasks to the queue and claim items for 30 seconds.
   *
   * @validate-module-enabled rules,rules_scheduler
   *
   * @see rules_scheduler_cron()
   * @see \Drupal\Core\Cron::run()
   * @see \Drupal\rules_scheduler\Plugin\QueueWorker\TaskWorker
   */
  #[CLI\Command(name: 'rules:scheduler-tasks', aliases: ['rusch', 'rules-scheduler-tasks'])]
  #[CLI\Help(description: 'Checks for scheduled tasks to be added to the queue.')]
  #[CLI\Usage(name: 'drush rules:scheduler-tasks', description: 'Add scheduled tasks to the queue.')]
  #[CLI\Usage(name: 'drush rules:scheduler-tasks --claim', description: 'Add scheduled tasks to the queue and claim items for the default amount of time.')]
  #[CLI\Usage(name: 'drush rules:scheduler-tasks --claim=30', description: 'Add scheduled tasks to the queue and claim items for 30 seconds.')]
  public function schedulerTasks(array $options = ['claim' => FALSE]): void {

    // Force Rules Scheduler to queue any ready-to-execute tasks.
    // This is exactly what Rules Scheduler does on a cron run, but
    // note this is being done OUTSIDE of hook_cron() - cron doesn't
    // trigger this, drush does.
    $this->rulesSchedulerManager->queueTasks();

    // Much of this code is taken from \Drupal\Core\Cron::processQueues().
    // It would be nice to avoid this duplication.
    $claim = $options['claim'];
    if ($claim) {
      // Fetch the queue information and worker plugin definition.
      $queue_name = 'rules_scheduler_tasks';
      $info = $this->queueWorkerManager->getDefinition($queue_name);

      // The drush option can override the default process time.
      $default_time = isset($info['cron']['time']) ? isset($info['cron']['time']) : 15;
      $lease_time = is_numeric($claim) ? (int) $claim : $default_time;
      $end = time() + $lease_time;

      // Claim items and process the queue.
      $queue = $this->queueFactory->get($queue_name);
      $queue_worker = $this->queueWorkerManager->createInstance('rules_scheduler_tasks');

      $claimed = 0;
      while (time() < $end && ($item = $queue->claimItem($lease_time))) {
        try {
          $queue_worker->processItem($item->data);
          $queue->deleteItem($item);
          $claimed++;
        }
        catch (RequeueException $e) {
          // The worker requested the task be immediately requeued.
          $queue->releaseItem($item);
        }
        catch (SuspendQueueException $e) {
          // If the worker indicates there is a problem with the whole queue,
          // release the item and skip to the next queue.
          $queue->releaseItem($item);
          $this->logger()->error($e);
          // Skip processing this queue.
          continue;
        }
        catch (\Exception $e) {
          // In case of any other kind of exception, log it and leave the item
          // in the queue to be processed again later.
          $this->logger()->error($e);
        }
      }

      if ($claimed) {
        $this->logger()->info(dt('Claimed and worked on @claimed scheduled tasks for up to @time seconds.', ['@claimed' => $claimed, '@time' => $lease_time]));
      }
    }
  }

}
