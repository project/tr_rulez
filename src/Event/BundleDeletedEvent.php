<?php

namespace Drupal\tr_rulez\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Represent various entity bundle events.
 *
 * @see rules_entity_presave()
 */
class BundleDeletedEvent extends Event {

  const EVENT_NAME = 'tr_rulez.entity_bundle_delete';

  /**
   * The entity type name. 'node', 'user', etc.
   *
   * @var string
   *
   * @phpcs:disable Drupal.NamingConventions.ValidVariableName.LowerCamelName
   */
  public $entity_type;
  // phpcs:enable

  /**
   * The bundle name.
   *
   * @var string
   *
   * @phpcs:disable Drupal.NamingConventions.ValidVariableName.LowerCamelName
   */
  public $bundle_name;
  // phpcs:enable

  /**
   * Constructs the object.
   *
   * @param string $entity_type
   *   The entity type name.
   * @param string $bundle_name
   *   The bundle name.
   */
  public function __construct($entity_type, $bundle_name) {
    $this->entity_type = $entity_type;
    $this->bundle_name = $bundle_name;
  }

}
