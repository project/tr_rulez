<?php

declare(strict_types=1);

namespace Drupal\tr_rulez\Hook;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\tr_rulez\Event\BundleCreatedEvent;
use Drupal\tr_rulez\Event\BundleDeletedEvent;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Hook implementations used to create and dispatch Entity Events.
 */
final class TrRulezEntityHooks {

  /**
   * Constructs a new TrRulezEntityHooks service.
   *
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher
   *   The event_dispatcher service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config.factory service.
   */
  public function __construct(
    protected EventDispatcherInterface $dispatcher,
    protected ConfigFactoryInterface $configFactory,
  ) {}

  /**
   * Implements hook_entity_type_alter().
   *
   * This hook may be used to alter or override the behavior of the two Rules
   * configuration entities: rules_reaction_rule and rules_component.
   *
   * The operations that may be performed are listed in the documentation
   * for \Drupal\Core\Entity\EntityTypeInterface.
   */
  #[Hook('entity_type_alter')]
  public function entityTypeAlter(array &$entity_types): void {
    $config = $this->configFactory->get('tr_rulez.settings');
    if ($config->get('ui_choice') == 1) {
      /** @var \Drupal\Core\Entity\EntityTypeInterface[] $entity_types */
      $entity_types['rules_reaction_rule']->setListBuilderClass('Drupal\tr_rulez\Controller\RulesReactionListBuilder');
      // phpcs:ignore Drupal.Files.LineLength.TooLong
      // $entity_types['rules_reaction_rule']->setFormClass('edit', 'Drupal\tr_rulez\Form\ReactionRuleEditForm');
      $entity_types['rules_reaction_rule']->setLinkTemplate('clone', '/admin/config/workflow/rules/reactions/clone/{rules_reaction_rule}');

      $entity_types['rules_component']->setListBuilderClass('Drupal\tr_rulez\Controller\RulesComponentListBuilder');
      $entity_types['rules_component']->setLinkTemplate('clone', '/admin/config/workflow/rules/reactions/clone/{rules_component}');
    }
    else {
      // Use the default Rules UI.
    }
  }

  /**
   * Implements hook_entity_bundle_create().
   */
  #[Hook('entity_bundle_create')]
  public function entityBundleCreate(string $entity_type_name, string $bundle_name): void {
    if ($this->dispatcher->hasListeners(BundleCreatedEvent::EVENT_NAME)) {
      $event = new BundleCreatedEvent($entity_type_name, $bundle_name);
      $this->dispatcher->dispatch($event, $event::EVENT_NAME);
    }
  }

  /**
   * Implements hook_entity_bundle_delete().
   */
  #[Hook('entity_bundle_delete')]
  public function entityBundleDelete(string $entity_type_name, string $bundle_name): void {
    if ($this->dispatcher->hasListeners(BundleDeletedEvent::EVENT_NAME)) {
      $event = new BundleDeletedEvent($entity_type_name, $bundle_name);
      $this->dispatcher->dispatch($event, $event::EVENT_NAME);
    }
  }

}
