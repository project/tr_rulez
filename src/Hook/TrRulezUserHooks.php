<?php

declare(strict_types=1);

namespace Drupal\tr_rulez\Hook;

use Drupal\Core\Hook\Attribute\Hook;
use Drupal\tr_rulez\Event\UserWasBlockedEvent;
use Drupal\tr_rulez\Event\UserWasUnblockedEvent;
use Drupal\user\UserInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Hook implementations used to create and dispatch User Events.
 */
final class TrRulezUserHooks {

  /**
   * Constructs a new TrRulezUserHooks service.
   *
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $dispatcher
   *   The event_dispatcher service.
   */
  public function __construct(
    protected EventDispatcherInterface $dispatcher,
  ) {}

  /**
   * Implements hook_ENTITY_TYPE_update() for the User entity.
   */
  #[Hook('user_update')]
  public function userUpdate(UserInterface $account): void {
    /** @var \Drupal\user\UserInterface $original */
    $original = $account->original;

    if (!$original->isBlocked() && $account->isBlocked()) {
      if ($this->dispatcher->hasListeners(UserWasBlockedEvent::EVENT_NAME)) {
        // The user has been blocked.
        $event = new UserWasBlockedEvent($account);
        $this->dispatcher->dispatch($event, $event::EVENT_NAME);
      }
    }
    elseif ($original->isBlocked() && !$account->isBlocked()) {
      if ($this->dispatcher->hasListeners(UserWasUnblockedEvent::EVENT_NAME)) {
        // The user has been unblocked.
        $event = new UserWasUnblockedEvent($account);
        $this->dispatcher->dispatch($event, $event::EVENT_NAME);
      }
    }
  }

}
