<?php

declare(strict_types=1);

namespace Drupal\tr_rulez\Hook;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Hook implementations of Rules hooks.
 */
final class TrRulezRulesHooks {
  use StringTranslationTrait;

  /**
   * Constructs a new TrRulezRulesHooks service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config.factory service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    TranslationInterface $string_translation,
  ) {
    $this->stringTranslation = $string_translation;
  }

  /**
   * Implements hook_rules_expression_info_alter().
   */
  #[Hook('rules_expression_info_alter')]
  public function rulesExpressionInfoAlter(array &$definitions): void {
    $config = $this->configFactory->get('tr_rulez.settings');
    if ($config->get('ui_choice') == 1) {
      // Modify form_class definitions.
      $definitions['rules_rule']['class'] = 'Drupal\tr_rulez\Plugin\RulesExpression\RuleExpression';
      $definitions['rules_action_set']['form_class'] = 'Drupal\tr_rulez\Form\Expression\ActionContainerForm';
      $definitions['rules_and']['form_class'] = 'Drupal\tr_rulez\Form\Expression\ConditionContainerForm';
      $definitions['rules_or']['form_class'] = 'Drupal\tr_rulez\Form\Expression\ConditionContainerForm';
      $definitions['rules_loop']['form_class'] = 'Drupal\tr_rulez\Form\Expression\ActionContainerForm';

      // Add descriptions.
      $definitions['rules_action']['description'] = $this->t("An Action component defines an executable action expression. This component is used to wrap action plugins and is responsible to setup all the context necessary, instantiate the action plugin and to execute it.");
      $definitions['rules_action_set']['description'] = $this->t("The Action Set component hold a set of actions and executes all of them unconditionally.");
      $definitions['rules_condition']['description'] = $this->t("A Condition component defines an executable condition expression. This component is used to wrap condition plugins and is responsible to setup all the context necessary, instantiate the condition plugin and to execute it.");
      $definitions['rules_and']['description'] = $this->t("Condition set (AND) evaluates a group of conditions with a logical AND. This means the set evaluates to TRUE only when all of the conditions are TRUE.");
      $definitions['rules_or']['description'] = $this->t("Condition set (OR) evaluates a group of conditions with a logical OR. This means the set evaluates to TRUE when one or more of the conditions is TRUE.");
      $definitions['rules_loop']['description'] = $this->t("A Loop component iterates over a list and executes a set of actions for each element of that list.");
    }
    else {
      // Use the default Rules UI.
    }
  }

}
