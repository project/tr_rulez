<?php

declare(strict_types=1);

namespace Drupal\tr_rulez\Hook;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Hook implementations used to alter and enhance forms.
 */
final class TrRulezFormHooks {
  use StringTranslationTrait;

  /**
   * Constructs a new TrRulezFormHooks service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config.factory service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    TranslationInterface $string_translation,
  ) {
    $this->stringTranslation = $string_translation;
  }

  /**
   * Implements hook_form_FORM_ID_alter() for rules_settings_form.
   */
  #[Hook('form_rules_settings_form_alter')]
  public function rulesSettingsFormAlter(array &$form, FormStateInterface $form_state): void {
    $config = $this->configFactory->get('tr_rulez.settings');
    $build['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('UI options'),
    ];
    $build['settings']['ui_choice'] = [
      '#type' => 'radios',
      '#options' => [
        0 => $this->t('Rules default UI'),
        1 => $this->t('Rules Essentials improved UI'),
      ],
      '#default_value' => $config->get('ui_choice'),
      '#description' => $this->t('Choose UI style for creating, editing, and listing Reaction Rules and Rules Components.'),
    ];
    $form['#submit'][] = [$this, 'rulesSettingsFormSubmit'];

    $form = $build + $form;
  }

  /**
   * Additional submit handler for rules_settings_form.
   */
  public function rulesSettingsFormSubmit(array $form, FormStateInterface $form_state): void {
    $config = $this->configFactory->getEditable('tr_rulez.settings');

    // Check to see if UI choice was changed. If so, we need to save the new
    // selection then clear the cache to force the UI changes to take effect.
    if ($config->get('ui_choice') != $form_state->getValue(['settings', 'ui_choice'])) {
      $config->set('ui_choice', $form_state->getValue(['settings', 'ui_choice']))->save();
      drupal_flush_all_caches();
    }
  }

}
