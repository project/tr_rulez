<?php

declare(strict_types=1);

namespace Drupal\tr_rulez\Drush\Commands;

use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;

/**
 * Drush 12+ commands for the Rules Essentials module.
 */
final class TrRulezDrushCommands extends DrushCommands {

  /**
   * Shows a list of available fields.
   *
   * @command typed-data:fields
   * @aliases field-list
   */
  #[CLI\Command(name: 'typed-data:fields', aliases: ['field-list'])]
  #[CLI\Help(description: 'Shows a list of available fields.')]
  public function listFields(): void {
    $this->formatOutput('plugin.manager.field.field_type', 'Available Field plugins:', FALSE);
  }

  /**
   * Helper function to format command output.
   *
   * @param string $plugin_manager_service
   *   The service name to use.
   * @param string $title
   *   The output title.
   * @param bool $categories
   *   Whether to group output into categories or not.
   * @param bool $short
   *   TRUE to display short form of output.
   */
  protected function formatOutput(string $plugin_manager_service, string $title, bool $categories = TRUE, bool $short = FALSE): void {
    // phpcs:ignore DrupalPractice.Objects.GlobalDrupal.GlobalDrupal
    $definitions = \Drupal::service($plugin_manager_service)->getDefinitions();
    $plugins = [];
    foreach ($definitions as $plugin) {
      if ($categories) {
        if ($short) {
          $plugins[(string) $plugin['category']][] = $plugin['id'];
        }
        else {
          $plugins[(string) $plugin['category']][] = $plugin['label'] . '   (' . $plugin['id'] . ')';
        }
      }
      else {
        if ($short) {
          $plugins[] = $plugin['id'];
        }
        else {
          $plugins[] = $plugin['label'] . '   (' . $plugin['id'] . ')';
        }
      }
    }

    $this->output()->writeln(dt($title));
    if ($categories) {
      ksort($plugins);
      foreach ($plugins as $category => $plugin_list) {
        $this->output()->writeln('  ' . $category);
        sort($plugin_list);
        $this->output()->writeln('    ' . implode(PHP_EOL . '    ', $plugin_list));
        $this->output()->writeln('');
      }
    }
    else {
      $unique = array_unique($plugins);
      sort($unique);
      $this->output()->writeln('  ' . implode(PHP_EOL . '  ', $unique) . PHP_EOL);
    }
  }

}
