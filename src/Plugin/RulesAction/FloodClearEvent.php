<?php

declare(strict_types=1);

namespace Drupal\tr_rulez\Plugin\RulesAction;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\rules\Context\ContextDefinition;
use Drupal\rules\Core\Attribute\RulesAction;

/**
 * Provides the 'Clear flood event' action.
 *
 * Makes the flood control mechanism forget an event for the current visitor.
 *
 * @RulesAction(
 *   id = "rules_flood_clear_event",
 *   label = @Translation("Clear flood event"),
 *   category = @Translation("System"),
 *   context_definitions = {
 *     "name" = @ContextDefinition("string",
 *       label = @Translation("Event name"),
 *       description = @Translation("The unique name of the flood event.")
 *     ),
 *     "identifier" = @ContextDefinition("string",
 *       label = @Translation("Identifier"),
 *       description = @Translation("Unique identifier of the current user. Defaults to the current user's IP address."),
 *       default_value = NULL,
 *       required = FALSE
 *     ),
 *   }
 * )
 */
#[RulesAction(
  id: "rules_flood_clear_event",
  label: new TranslatableMarkup("Clear flood event"),
  category: new TranslatableMarkup("System"),
  context_definitions: [
    "name" => new ContextDefinition(
      data_type: "string",
      label: new TranslatableMarkup("Event name"),
      description: new TranslatableMarkup("The unique name of the flood event.")
    ),
    "identifier" => new ContextDefinition(
      data_type: "string",
      label: new TranslatableMarkup("Identifier"),
      description: new TranslatableMarkup("Unique identifier of the current user. Defaults to the current user's IP address."),
      default_value: NULL,
      required: FALSE
    ),
  ]
)]
class FloodClearEvent extends RulesFloodActionBase {

  /**
   * Makes the flood control mechanism forget an event for the current visitor.
   *
   * @param string $name
   *   The name of a flood event.
   * @param string $identifier
   *   (optional) Unique identifier of the current user. Defaults to the current
   *   user's IP address.
   */
  protected function doExecute($name, $identifier = NULL) {
    $this->flood->clear($name, $identifier);
    $this->logger->notice('Cleared flood event %name with identifier=%identifier', [
      '%name' => $name,
      '%identifier' => $identifier,
    ]);
  }

}
