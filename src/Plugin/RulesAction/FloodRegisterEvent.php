<?php

declare(strict_types=1);

namespace Drupal\tr_rulez\Plugin\RulesAction;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\rules\Context\ContextDefinition;
use Drupal\rules\Core\Attribute\RulesAction;

/**
 * Registers an event for the current visitor to the flood control mechanism.
 *
 * @RulesAction(
 *   id = "rules_flood_register_event",
 *   label = @Translation("Register flood event"),
 *   category = @Translation("System"),
 *   context_definitions = {
 *     "name" = @ContextDefinition("string",
 *       label = @Translation("Event name"),
 *       description = @Translation("The unique name of the flood event.")
 *     ),
 *     "window" = @ContextDefinition("integer",
 *       label = @Translation("Window"),
 *       description = @Translation("Number of seconds in the time window for this event."),
 *       default_value = 3600,
 *       required = FALSE
 *     ),
 *     "identifier" = @ContextDefinition("string",
 *       label = @Translation("Identifier"),
 *       description = @Translation("Unique identifier of the current user. Defaults to their IP address."),
 *       default_value = NULL,
 *       required = FALSE
 *     ),
 *   }
 * )
 */
#[RulesAction(
  id: "rules_flood_register_event",
  label: new TranslatableMarkup("Register flood event"),
  category: new TranslatableMarkup("System"),
  context_definitions: [
    "name" => new ContextDefinition(
      data_type: "string",
      label: new TranslatableMarkup("Event name"),
      description: new TranslatableMarkup("The unique name of the flood event.")
    ),
    "window" => new ContextDefinition(
      data_type: "integer",
      label: new TranslatableMarkup("Window"),
      description: new TranslatableMarkup("Number of seconds in the time window for this event."),
      default_value: 3600,
      required: FALSE
    ),
    "identifier" => new ContextDefinition(
      data_type: "string",
      label: new TranslatableMarkup("Identifier"),
      description: new TranslatableMarkup("Unique identifier of the current user. Defaults to the current user's IP address."),
      default_value: NULL,
      required: FALSE
    ),
  ]
)]
class FloodRegisterEvent extends RulesFloodActionBase {

  /**
   * Registers an event for the current visitor to the flood control mechanism.
   *
   * @param string $name
   *   The name of a flood event.
   * @param int $window
   *   (optional) Number of seconds in the time window for this event (default
   *   is 3600 seconds, or 1 hour).
   * @param string $identifier
   *   (optional) Unique identifier of the current user. Defaults to the current
   *   user's IP address.
   */
  protected function doExecute($name, int $window = 3600, $identifier = NULL) {
    $this->flood->register($name, $window, $identifier);
    $this->logger->notice('Registered flood event %name with window=%window and identifier=%identifier', [
      '%name' => $name,
      '%window' => $window,
      '%identifier' => $identifier,
    ]);
  }

}
