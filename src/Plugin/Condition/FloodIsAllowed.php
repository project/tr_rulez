<?php

namespace Drupal\tr_rulez\Plugin\Condition;

use Drupal\Core\Flood\FloodInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\rules\Context\ContextDefinition;
use Drupal\rules\Core\Attribute\Condition;
use Drupal\rules\Core\RulesConditionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an 'Flood event is allowed' condition.
 *
 * @Condition(
 *   id = "rules_flood_is_allowed",
 *   label = @Translation("Flood event is allowed"),
 *   category = @Translation("System"),
 *   context_definitions = {
 *     "name" = @ContextDefinition("string",
 *       label = @Translation("Flood event allowed"),
 *       description = @Translation("The unique name of the flood event.")
 *     ),
 *     "threshold" = @ContextDefinition("integer",
 *       label = @Translation("Threshold"),
 *       description = @Translation("The maximum number of times each user can do this event per time window.")
 *     ),
 *     "window" = @ContextDefinition("integer",
 *       label = @Translation("Window"),
 *       description = @Translation("Number of seconds in the time window for this event (default is 3600 seconds, or 1 hour)."),
 *       default_value = 3600,
 *       required = FALSE
 *     ),
 *     "identifier" = @ContextDefinition("string",
 *       label = @Translation("Identifier"),
 *       description = @Translation("Unique identifier of the current user. Defaults to the current user's IP address."),
 *       default_value = NULL,
 *       required = FALSE
 *     ),
 *   }
 * )
 */
#[Condition(
  id: "rules_flood_is_allowed",
  label: new TranslatableMarkup("Flood event is allowed"),
  category: new TranslatableMarkup("System"),
  context_definitions: [
    "name" => new ContextDefinition(
      data_type: "string",
      label: new TranslatableMarkup("Flood event allowed"),
      description: new TranslatableMarkup("The unique name of the flood event.")
    ),
    "threshold" => new ContextDefinition(
      data_type: "integer",
      label: new TranslatableMarkup("Threshold"),
      description: new TranslatableMarkup("The maximum number of times each user can do this event per time window.")
    ),
    "window" => new ContextDefinition(
      data_type: "integer",
      label: new TranslatableMarkup("Window"),
      description: new TranslatableMarkup("Number of seconds in the time window for this event (default is 3600 seconds, or 1 hour)."),
      default_value: 3600,
      required: FALSE
    ),
    "identifier" => new ContextDefinition(
      data_type: "string",
      label: new TranslatableMarkup("Identifier"),
      description: new TranslatableMarkup("Unique identifier of the current user. Defaults to the current user's IP address."),
      default_value: NULL,
      required: FALSE
    ),
  ]
)]
class FloodIsAllowed extends RulesConditionBase implements ContainerFactoryPluginInterface {

  /**
   * The flood service.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected $flood;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('flood')
    );
  }

  /**
   * Constructs a FloodIsAllowed object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Flood\FloodInterface $flood
   *   The flood service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FloodInterface $flood) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->flood = $flood;
  }

  /**
   * Checks if the flood event is allowed.
   *
   * @param string $name
   *   The name of a flood event.
   * @param int $threshold
   *   The maximum number of times each user can do this event per time window.
   * @param int $window
   *   (optional) Number of seconds in the time window for this event (default
   *   is 3600 seconds, or 1 hour).
   * @param string $identifier
   *   (optional) Unique identifier of the current user. Defaults to the current
   *   user's IP address.
   *
   * @return bool
   *   TRUE if the user is allowed to proceed. FALSE if they have exceeded the
   *   threshold and should not be allowed to proceed.
   */
  protected function doEvaluate($name, int $threshold, int $window = 3600, $identifier = NULL) {
    return $this->flood->isAllowed($name, $threshold, $window, $identifier);
  }

}
