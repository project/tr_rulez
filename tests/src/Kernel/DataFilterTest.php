<?php

declare(strict_types=1);

namespace Drupal\Tests\tr_rulez\Kernel;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests using typed data filters.
 *
 * @group tr_rulez
 *
 * @coversDefaultClass \Drupal\typed_data\DataFilterManager
 */
class DataFilterTest extends KernelTestBase {

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected $typedDataManager;

  /**
   * The data filter manager.
   *
   * @var \Drupal\typed_data\DataFilterManagerInterface
   */
  protected $dataFilterManager;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['tr_rulez', 'rules', 'typed_data'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->typedDataManager = $this->container->get('typed_data_manager');
    $this->dataFilterManager = $this->container->get('plugin.manager.typed_data_filter');
  }

  /**
   * Tests the operation of the 'link' data filter.
   *
   * @covers \Drupal\tr_rulez\Plugin\TypedDataFilter\LinkFilter
   */
  public function testLinkFilter(): void {
    $filter = $this->dataFilterManager->createInstance('link');
    $data = $this->typedDataManager->create(DataDefinition::create('uri'), 'https://www.example.com/');

    $this->assertTrue($filter->canFilter($data->getDataDefinition()));
    $this->assertFalse($filter->canFilter(DataDefinition::create('any')));

    $this->assertEquals('string', $filter->filtersTo($data->getDataDefinition(), [])->getDataType());

    // Verify that one argument is required.
    $fails = $filter->validateArguments($data->getDataDefinition(), [/* No arguments given */]);
    $this->assertEquals(1, count($fails));
    $this->assertStringContainsString('Missing arguments for filter', (string) $fails[0]);

    // @todo Fix core bug in PrimitiveTypeConstraintValidator.php
    // Specifically, Creating a 'uri' data definition should not crash if
    // passed a non-string argument (such as an object). Instead, it should
    // fail gracefully with a type error like all the other data type
    // definitions do.
    // phpcs:ignore Drupal.Files.LineLength.TooLong
    // $fails = $filter->validateArguments($data->getDataDefinition(), [new \stdClass()]);
    $fails = $filter->validateArguments($data->getDataDefinition(), ['123']);
    $this->assertEquals(1, count($fails));
    $this->assertEquals('This value should be of the correct primitive type.', $fails[0]);

    $this->assertEquals('<a href="https://www.example.com/">link text</a>', $filter->filter($data->getDataDefinition(), $data->getValue(), ['link text']));
  }

  /**
   * Tests the operation of the 'raw' data filter.
   *
   * @covers \Drupal\tr_rulez\Plugin\TypedDataFilter\RawFilter
   */
  public function testRawFilter(): void {
    $filter = $this->dataFilterManager->createInstance('raw');
    $data = $this->typedDataManager->create(DataDefinition::create('string'), '<b>Test <em>raw</em> filter</b>');

    $this->assertTrue($filter->canFilter($data->getDataDefinition()));
    $this->assertFalse($filter->canFilter(DataDefinition::create('any')));

    $this->assertEquals('string', $filter->filtersTo($data->getDataDefinition(), [])->getDataType());

    $this->assertEquals('<b>Test <em>raw</em> filter</b>', $filter->filter($data->getDataDefinition(), $data->getValue(), []));
  }

}
