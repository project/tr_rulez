<?php

declare(strict_types=1);

namespace Drupal\Tests\tr_rulez\Kernel;

use Drupal\Tests\rules\Kernel\RulesKernelTestBase;

/**
 * Functional tests for the flood control mechanism.
 *
 * @group tr_rulez
 */
class FloodTest extends RulesKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'rules', 'tr_rulez'];

  /**
   * The flood service.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected $flood;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->flood = $this->container->get('flood');
  }

  /**
   * Test rules_flood_is_allowed condition.
   *
   * Test is performed using the flood service directly to perform
   * the actions, so that only this Rules condition is being tested.
   */
  public function testFloodIsAllowedCondition(): void {
    $name = 'tr_rulez.flood_test_is_allowed';
    $threshold = 1;
    $window_expired = -1;

    $cron = $this->container->get('cron');

    /** @var \Drupal\rules\Core\RulesConditionInterface $condition */
    $is_allowed_condition = $this->container->get('plugin.manager.condition')
      ->createInstance('rules_flood_is_allowed');
    $is_allowed_condition->setContextValue('name', $name)
      ->setContextValue('threshold', $threshold);

    // No events yet, so event should be allowed.
    $this->assertTrue($is_allowed_condition->evaluate());

    // Register expired event.
    $this->flood->register($name, $window_expired);
    // Verify event is not allowed anymore,
    // since event is still in the flood table.
    $this->assertFalse($is_allowed_condition->evaluate());

    // Run cron to removed expired event from the flood table,
    // then verify event is now allowed.
    $cron->run();
    $this->assertTrue($is_allowed_condition->evaluate());

    // Register unexpired event.
    $this->flood->register($name);
    // Verify event is not allowed.
    $this->assertFalse($is_allowed_condition->evaluate());

    // Run cron and verify event is still not allowed.
    $cron->run();
    $this->assertFalse($is_allowed_condition->evaluate());
  }

  /**
   * Test rules_flood_register_event action.
   *
   * Test is performed using the flood service directly to check
   * the condition, so that only this Rules action is being tested.
   */
  public function testFloodRegisterEventAction(): void {
    $name = 'tr_rulez.flood_test_register_event';
    $threshold = 1;
    $window_expired = -1;

    $cron = $this->container->get('cron');

    /** @var \Drupal\rules\Core\RulesActionInterface $register_action */
    $register_action = \Drupal::service('plugin.manager.rules_action')
      ->createInstance('rules_flood_register_event');
    $register_action->setContextValue('name', $name)
      ->setContextValue('window', $window_expired);

    // No events yet, so event should be allowed.
    $this->assertTrue($this->flood->isAllowed($name, $threshold));

    // Register expired event.
    $register_action->execute();

    // Verify event is not allowed anymore,
    // since event is still in the flood table.
    $this->assertFalse($this->flood->isAllowed($name, $threshold));

    // Run cron to removed expired event from the flood table,
    // then verify event is now allowed.
    $cron->run();
    $this->assertTrue($this->flood->isAllowed($name, $threshold));

    // Register unexpired event.
    $register_action->setContextValue('name', $name)
      ->setContextValue('window', 3600);
    $register_action->execute();

    // Verify event is not allowed.
    $this->assertFalse($this->flood->isAllowed($name, $threshold));

    // Run cron and verify event is still not allowed.
    $cron->run();
    $this->assertFalse($this->flood->isAllowed($name, $threshold));
  }

  /**
   * Test rules_flood_clear_event action.
   *
   * Test is performed using the flood service directly to check
   * the condition, so that only this Rules action is being tested.
   */
  public function testFloodClearEventAction(): void {
    $name = 'tr_rulez.flood_test_clear_event';
    $threshold = 1;

    /** @var \Drupal\rules\Core\RulesActionInterface $clear_action */
    $clear_action = \Drupal::service('plugin.manager.rules_action')
      ->createInstance('rules_flood_clear_event');
    $clear_action->setContextValue('name', $name);

    // Register unexpired event.
    $this->flood->register($name);

    // Verify event is not allowed.
    $this->assertFalse($this->flood->isAllowed($name, $threshold));

    // Clear event and verify event is now allowed.
    $clear_action->execute();
    $this->assertTrue($this->flood->isAllowed($name, $threshold));
  }

}
