<?php

declare(strict_types=1);

namespace Drupal\Tests\tr_rulez\Unit\Integration\Event;

/**
 * Checks that the event "tr_rulez.user_was_unblocked" is correctly defined.
 *
 * @coversDefaultClass \Drupal\tr_rulez\Event\UserWasUnblockedEvent
 * @group tr_rulez
 */
class UserWasUnblockedTest extends EventTestBase {

  /**
   * Tests the event metadata.
   */
  public function testUserWasUnblockedEvent(): void {
    $event = $this->eventManager->createInstance('tr_rulez.user_was_unblocked');
    $user_context_definition = $event->getContextDefinition('account');
    $this->assertSame('entity:user', $user_context_definition->getDataType());
    $this->assertSame('Updated user', $user_context_definition->getLabel());
  }

}
